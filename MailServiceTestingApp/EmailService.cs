﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace MailServiceTestingApp
{
    public class EmailService
    {
        #region Constructor & Default Data
        private NetworkCredential networkCredential = new NetworkCredential("", "");
        private string fromEmail = "";
        private string fromName = "";
        private string host = "";
        private int port = 587;
        private bool EnableSsl = false;

        public EmailService(string host, string userName, string password, string fromEmail, string fromName, int port, bool EnableSsl)
        {
            if (userName != null && password != null)
            {
                networkCredential = !string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(userName) ? new NetworkCredential { UserName = userName, Password = password } : networkCredential;

                this.fromEmail = fromEmail;
                this.fromName = fromName;
                this.host = host;
                this.port =  port;
                this.EnableSsl = EnableSsl;
            }
        }
        #endregion

        public bool SendMailWithMultipleMemoryStreamFileAttachment(List<VmEmailAttactment> attactments, string toEmail, string subject, string body, string fromEmail = null, string fromName = null, string ccEmail = null, string bccEmail = null)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMsg = new MailMessage();
            try
            {
                fromEmail = this.fromEmail;
                fromName = this.fromName;
                List<MemoryStream> memoStreamList = new List<MemoryStream>();
                if (attactments != null)
                {
                    foreach (var att in attactments)
                    {
                        try
                        {
                            MemoryStream mst = new MemoryStream(att.mailStreamAttachment);
                            mailMsg.Attachments.Add(new Attachment(mst, att.attachmentFileName));
                            memoStreamList.Add(mst);
                        }
                        catch
                        {
                            throw;
                        }
                    }
                }
                mailMsg.From = new MailAddress(fromEmail, fromName);
                mailMsg.To.Add(toEmail);
                mailMsg.Subject = subject;
                mailMsg.IsBodyHtml = true;
                mailMsg.BodyEncoding = Encoding.UTF8;
                mailMsg.Body = body;
                mailMsg.Priority = MailPriority.Normal;
                if (!string.IsNullOrEmpty(bccEmail))
                {
                    MailAddress addressBCC = new MailAddress(bccEmail);
                    mailMsg.Bcc.Add(addressBCC);
                }
                if (!string.IsNullOrEmpty(ccEmail))
                {
                    MailAddress addressCC = new MailAddress(ccEmail);
                    mailMsg.CC.Add(addressCC);
                }
                smtpClient.Credentials = networkCredential;
                smtpClient.Host = host;
                smtpClient.Port = port;
                smtpClient.EnableSsl = EnableSsl;
                smtpClient.Send(mailMsg);
                foreach (MemoryStream stream in memoStreamList)
                {
                    stream.Dispose();
                }
                return true;
            }
            catch (SmtpException smtpEx)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

    public class VmEmailAttactment
    {
        public string attachmentFileName { get; set; }
        public byte[] mailStreamAttachment { get; set; }
    }
}
