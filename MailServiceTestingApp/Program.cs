﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace MailServiceTestingApp
{
    public class Program
    {
        #region "Class"
        public class MailInformation
        {
            public static string GetProjectFileDirectoryPath(string fileNameWithPath)
            {
                string path = "";
                try
                {
                    string fileDirectoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                    if (fileDirectoryPath.Contains(@"\bin\"))
                    {
                        int upTo = fileDirectoryPath.LastIndexOf("bin");
                        string filePath = fileDirectoryPath.Substring(0, upTo);
                        path = filePath + @fileNameWithPath;
                    }
                    else
                        path = fileDirectoryPath + @fileNameWithPath;

                }
                catch (Exception ex) { path = ""; }
                return path;
            }

            public static string MailBody()
            {
                string mailBodyHtml = "";
                try
                {
                    string mailTemplateHTMLPath = GetProjectFileDirectoryPath(@"\MailBody\DummyMailTemplate.html");
                    mailBodyHtml = System.IO.File.ReadAllText(mailTemplateHTMLPath);
                }
                catch (Exception ex) { mailBodyHtml = ""; }
                return mailBodyHtml;
            }

            public static List<VmEmailAttactment> MailAttachments()
            {
                var vmEmailAttactments = new List<VmEmailAttactment>();
                try
                {
                    var pdfPath = GetProjectFileDirectoryPath(@"\MailAttachment\DummyAttachmentOne.pdf");
                    byte[] pdfByte = File.ReadAllBytes(pdfPath);
                    vmEmailAttactments.Add(new VmEmailAttactment { attachmentFileName = "DummyAttachmentOne.pdf", mailStreamAttachment = pdfByte });

                    string excelPath = GetProjectFileDirectoryPath(@"\MailAttachment\DummyAttachmentTwo.xls");
                    byte[] excelByte = File.ReadAllBytes(excelPath);
                    vmEmailAttactments.Add(new VmEmailAttactment { attachmentFileName = "DummyAttachmentTwo.xls", mailStreamAttachment = excelByte });
                }
                catch (Exception ex) { vmEmailAttactments = new List<VmEmailAttactment>(); }
                return vmEmailAttactments;
            }
        }
        #endregion

        static void Main(string[] args)
        {
            bool result = false;
            try
            {
                int port = 26;
                bool EnableSsl = false;
                string host = "test.xyz.com";
                string fromName = "test.xyz.com";
                string username = "test.xyz.com";
                string password = "test.xyz.com";
                string fromEmail = "test.xyz.com";
                string companyName = "test.xyz.com";
                string sendToEmail = "test.xyz.com";
                string mailSubject = "test.xyz.com";

                var emailService = new EmailService(host, username, password, fromEmail, fromName, port, EnableSsl);
                result = emailService.SendMailWithMultipleMemoryStreamFileAttachment(MailInformation.MailAttachments(), sendToEmail, mailSubject, MailInformation.MailBody(), null, companyName, null, null);
                Console.WriteLine("Result: " + result);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Result: " + result + "(" + ex.Message + ")");
                Console.ReadKey();
            }
        }
    }
}
